﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JumpPad : MonoBehaviour {

	float power = 0;
	public float minpower = 0;
	public float maxpower = 100f;
	public Slider powerSlider;
	List<Rigidbody> balllist;
	bool ballready;
	// Use this for initialization
	void Start () {
		powerSlider.minValue = 0f;
		powerSlider.maxValue = maxpower;
		balllist= new List<Rigidbody>();
		//powerSlider.
	}
	
	// Update is called once per frame
	void Update () {

				if(ballready==true){
					powerSlider.gameObject.SetActive(true);
				}else{
					powerSlider.gameObject.SetActive(false);

				}
				powerSlider.value = power;
				if(balllist.Count>0){
					ballready = true;

					if(Input.GetKey(KeyCode.Space)){
							Debug.Log("space");

						if(power<= maxpower){
							power += 2000 * Time.deltaTime; 
						}
					}
					if(Input.GetKeyUp(KeyCode.Space)){
						foreach(Rigidbody r in balllist){
							r.AddForce(power* Vector3.up);
						}
					}
				}else{
					ballready=false;
					power=0;
				}

		
	}

	void OnTriggerEnter(Collider other){
		if(other.gameObject.CompareTag("Ball")){
			//Debug.Log("enter");
			balllist.Add(other.gameObject.GetComponent<Rigidbody>());
		}
	}
	void OnTriggerExit(Collider other){
		if(other.gameObject.CompareTag("Ball")){
			//Debug.Log("remove");

			balllist.Remove(other.gameObject.GetComponent<Rigidbody>());
			power=0;
		}


	}
	
}

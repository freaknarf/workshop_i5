﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explosion : MonoBehaviour {
	public float radius = 1;
    public float power = 1;

	// Use this for initialization
	
	
	// Update is called once per frame
	void Start () {
		/*Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null){
				rb.AddExplosionForce(power, explosionPos, radius, 3.0F);
			}

		}*/
		Debug.Log("cc");


	}

	private void OnCollisionEnter(Collision other)
	{
		Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null)
                rb.AddExplosionForce(power, explosionPos, radius, 1);
        }
	}

	void Update () {
		
	}
}

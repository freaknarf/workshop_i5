﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class TopScore : MonoBehaviour {
 
    //SerialPort sp = new SerialPort("/dev/ttyACM0", 9600); // set port of your arduino connected to computer
    
    public int bonus = 1;
    public string url = "http://arii.pe.hu/flipper_api.php";
    public string user_id;
    // Use this for initialization
    void Start() {
        user_id = PlayerPrefs.GetString("RegUser");
        StartCoroutine(Upload());
    }
 
    IEnumerator Upload() {

        
        List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        formData.Add( new MultipartFormDataSection("get_top_score=true&user_id="+user_id));

        string tempurl = url+"?get_top_score=true&user_id="+user_id;
        Debug.Log(url+"?get_score=true&user_id="+user_id);
        UnityWebRequest www = UnityWebRequest.Get(tempurl);
        yield return www.SendWebRequest();
 
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else {
            Debug.Log(www.downloadHandler.text);
            GameObject a = GameObject.FindWithTag("menu_top_score");
       	  	a.GetComponent<Text>().text  = www.downloadHandler.text ;


            //Debug.Log(www.text);
        }
    }

    
}

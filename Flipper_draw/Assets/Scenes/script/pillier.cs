﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class pillier : MonoBehaviour {
 
    //SerialPort sp = new SerialPort("/dev/ttyACM0", 9600); // set port of your arduino connected to computer
    
    public int bonus = 1;
    public string url = "http://arii.pe.hu/flipper_api.php";
    public string user_id;
    // Use this for initialization
    void Start() {
        user_id = PlayerPrefs.GetString("RegUser");
    }
 
    IEnumerator Upload() {

        GameObject a = GameObject.FindWithTag("solo_score_value");
        a.GetComponent<Text>().text  =(int.Parse(a.GetComponent<Text>().text) + 1 ).ToString();

        List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        formData.Add( new MultipartFormDataSection("update_score=true&score=1&user_id="+user_id));

        string tempurl = url+"?increment_score=true&score=1&user_id="+user_id;
        UnityWebRequest www = UnityWebRequest.Get(tempurl);
        yield return www.SendWebRequest();
 
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else {
            Debug.Log(www.downloadHandler.text);

            //Debug.Log(www.text);
        }
    }

    void OnCollisionEnter(){
        StartCoroutine(Upload());
    }
}

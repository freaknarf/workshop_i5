﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
 
public class ArduinoMove : MonoBehaviour {
 
	//file:///home/franck/T%C3%A9l%C3%A9chargements/Flipper_draw(1)/Flipper_draw/Assets/FlipperScript.cs
	SerialPort sp = new SerialPort("/dev/ttyACM1", 9600); // set port of your arduino connected to computer
 	public string inputName;
    

 	public float restPosition =0F;
	public float pressedPosition =45F;
	public float hitStrenght =10000000f;
	public float flipperDamper =15000f;
	public GameObject left;
	public GameObject right;
	public string inputNameL;
	public string inputNameR;

	HingeJoint hingeL;
	HingeJoint hingeR;

	// Use this for initialization
    void Start () {
        sp.Open();
        sp.ReadTimeout = 71;




        hingeL= left.GetComponent<HingeJoint>();
		hingeL.useSpring= true;

		hingeR= right.GetComponent<HingeJoint>();
		hingeR.useSpring= true;
	}
 
    void Update () {
		JointSpring spring = new JointSpring();
		spring.spring = hitStrenght;
		spring.damper = flipperDamper;
		int spValue=0;
		if (sp.IsOpen){
			try{
			spValue = sp.ReadByte();
			}catch (System.Exception){
				
			}
		}

		if((Input.GetAxis(inputNameL) ==1)
			|| (spValue==1)
		)
		{
			spring.targetPosition = -pressedPosition;

		}else{
			spring.targetPosition = -restPosition;
		}
		hingeL.spring = spring;
		hingeL.useLimits = true;

		spring = new JointSpring();
		spring.spring = hitStrenght;
		spring.damper = flipperDamper;
		if((Input.GetAxis(inputNameR) ==1)
		|| (spValue==2)
		)		
		{
			spring.targetPosition = pressedPosition;

		}else{
			spring.targetPosition = -restPosition;
		}
		hingeR.spring = spring;
		hingeR.useLimits = true;


    }
}
